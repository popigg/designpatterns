<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 22:49
 */

namespace iterator;

require_once 'Iterator.php';

class DinnerMenuIterator implements iterator
{
	var $items      = [];
	var $position   = 0;

	public function __construct($items)
	{
		$this->items = $items;
	}

	public function next()
	{
		$menuItem = $this->items[$this->position];
		$this->position = $this->position + 1;

		return $menuItem;
	}

	public function hasNext()
	{
		if ($this->position >= count($this->items) || !$this->items[$this->position])
		{
			return false;
		}
		return true;
	}
}