<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 22:55
 */

namespace command4;

class RemoteControl
{
	var $onCommands;
	var $offCommands;
	var $undoCommand;

	public function __construct()
	{
		$this->onCommands   = [];
		$this->offCommands  = [];
		$this->undoCommand  = null;
	}

	public function setCommand($slot, Command $onCommand, Command $offCommand )
	{
		$this->onCommands[$slot] = $onCommand;
		$this->offCommands[$slot] = $offCommand;
	}

	public function onButtonWasPressed($slot)
	{
		$this->onCommands[$slot]->execute();
		$this->undoCommand = $this->onCommands[$slot];
	}

	public function offButtonWasPressed($slot)
	{
		$this->offCommands[$slot]->execute();
		$this->undoCommand = $this->offCommands[$slot];
	}

	public function undoButtonWasPressed()
	{
		$this->undoCommand->undo();
	}

	public function __toString()
	{
		$string = PHP_EOL . '------------- Remote Control -----------------' . PHP_EOL;
		for ($i = 0; $i < count($this->onCommands); $i++){
			$string .= '[ slot ' . $i . ' ]' . get_class($this->onCommands[$i]) . '  ' . get_class($this->offCommands[$i]) . PHP_EOL;
		}

		$string .= '[ undo ] ' . get_class($this->undoCommand) . PHP_EOL;
		return $string;
	}

}