<?php

/**
 * User: popigg
 * Date: 22/02/15
 * Time: 11:54
 */

namespace AbstractFactory;


class ChicagoIngredient implements PizzaIngredientInterface
{
    public function createDough()
    {

    }

    public function createSauce()
    {

    }

    public function createCheese()
    {

    }

    public function createVeggies()
    {

    }

    public function createPepperoni()
    {

    }

    public function createClam()
    {

    }
}