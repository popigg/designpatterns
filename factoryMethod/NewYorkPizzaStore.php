<?php

namespace FactoryMethod;

require_once 'PizzaStore.php';
require_once 'NewYorkStyleCheesePizza.php';

class NewYorkPizzaStore extends PizzaStore
{
	public function createPizza( $item )
	{
		switch ( $item ) {
			case 'cheese':
				return new NewYorkStyleCheesePizza();
				break;

//			 case 'veggie':
//			 	return new NYStyleVeggiePizza();
//			 	break;
//
//			 case 'clam':
//			 	return new NYStyleClamPizza();
//			 	break;
//
//			 case 'pepperoni':
//			 	return new NYStylePepperoniPizza();
//			 	break;
					
			default:
				# code...
				break;
		}
	}
}