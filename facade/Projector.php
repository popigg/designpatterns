<?php
/**
 * User: popigg
 * Date: 04/04/15
 * Time: 00:05
 */

namespace facade;


class Projector
{
	public function on()
	{
		print('Top-O-Line Projector on' . PHP_EOL);
	}

	public function wideScreenMode()
	{
		print('Top-O-Line Projector widescreen mode (16:9 aspect ratio)' . PHP_EOL);
	}

	public function off()
	{
		print('Top-O-Line Projector off' . PHP_EOL);
	}
}