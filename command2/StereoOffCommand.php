<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 22:49
 */

namespace command2;

require_once 'Command.php';
require_once 'Stereo.php';

class StereoOffCommand implements Command
{
	var $stereo;

	public function __construct(Stereo $stereo)
	{
		$this->stereo = $stereo;
	}

	public function execute()
	{
		$this->stereo->off();
	}
}