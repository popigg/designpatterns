<?php

namespace Observer;


class ForecastDisplay implements Observer, Display {

    private $currentPressure = '29.92f';
    private $lastPressure;

    function __construct (Weather $weather) {
        $weather->registerObserver($this);
    }

    public function update($temp, $hum, $pres) {
        $this->lastPressure = $this->currentPressure;
        $this->currentPressure = $pres;

        $this->display();
    }

    public function display() {
        print("Forecast: ");

		if ($this->currentPressure > $this->lastPressure) {
            print("Improving weather on the way!" . "\xA");
        } else if ($this->currentPressure == $this->lastPressure ) {
            print("More of the same" . "\xA");
        } else if ($this->currentPressure < $this->lastPressure) {
            print("Watch out for cooler, rainy weather" . "\xA");
        }
    }
} 