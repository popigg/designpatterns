<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 22:41
 */

namespace iterator;


interface Iterator
{
	public function hasNext();

	public function next();
}