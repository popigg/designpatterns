<?php
/**
 * User: popigg
 * Date: 08/03/15
 * Time: 15:57
 */

namespace command;


class SimpleRemoteControl
{
	var $command;

	public function setCommand(Command $command)
	{
		$this->command = $command;
	}

	public function buttonWasPressed()
	{
		$this->command->execute();
	}
}