<?php
/**
 * User: popigg
 * Date: 08/03/15
 * Time: 15:46
 */

namespace command;

require_once 'Command.php';
require_once 'Light.php';

class LightOnCommand implements Command
{
	var $light;

	public function __construct( Light $light )
	{
		$this->light = $light;
	}

	public function execute()
	{
		$this->light->on();
	}
}