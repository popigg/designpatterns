<?php
/**
 * Created by PhpStorm.
 * User: popigg
 * Date: 21/11/14
 * Time: 16:52
 */

require_once ('SubjectInterface.php');
require_once ('ObserverInterface.php');
require_once ('DisplayInterface.php');
require_once ('Weather.php');
require_once ('CurrentConditionsDisplay.php');
require_once ('StaticticsDisplay.php');
require_once ('ForecastDisplay.php');
require_once ('HeatIndexDisplay.php');

$weather = new \Observer\Weather();

$currentConditionDisplay    = new \Observer\CurrentConditionsDisplay($weather);
$staticticsDisplay          = new \Observer\StaticticsDisplay($weather);
$forecastDisplay            = new \Observer\ForecastDisplay($weather);
$heatIndexDisplay           = new \Observer\HeatIndexDisplay($weather);

$weather->setMeasurements(80, 65, '30.4 f');
$weather->setMeasurements(82, 70, '29.2 f');
$weather->setMeasurements(78, 90, '29.2 f');