<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 23:26
 */

namespace iterator;

require_once 'PancakeHouseIterator.php';

class PancakeHouseMenu
{
	static $maxItems = 6;
	var $numOfItems = 0;
	var $items;

	public function __construct($items)
	{
		$this->items = $items;
	}

	public function createIterator()
	{
		return new PancakeHouseIterator($this->items);
	}
}