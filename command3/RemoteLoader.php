<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 23:15
 */

namespace command3;

require_once 'RemoteControlWithUndo.php';
require_once 'Light.php';
require_once 'LightOnCommand.php';
require_once 'LightOffCommand.php';


$remoteControl = new RemoteControlWithUndo();

$livingRoomLight    = new Light('Living Room');

$livingRoomLightOn  = new LightOnCommand($livingRoomLight);
$livingRoomLightOff = new LightOffCommand($livingRoomLight);


$remoteControl->setCommand(0, $livingRoomLightOn, $livingRoomLightOff);

$remoteControl->onButtonWasPressed(0);
$remoteControl->offButtonWasPressed(0);

print($remoteControl);

$remoteControl->undoButtonWasPressed();

$remoteControl->offButtonWasPressed(0);
$remoteControl->onButtonWasPressed(0);

print($remoteControl);

$remoteControl->undoButtonWasPressed();