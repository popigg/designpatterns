<?php

namespace AbstractFactory;

interface PizzaIngredientInterface {

    public function createDough();

    public function createSauce();

    public function createCheese();

    public function createVeggies();

    public function createPepperoni();

    public function createClam();

}