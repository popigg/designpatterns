<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 23:51
 */

use iterator\DinnerMenu;
use iterator\MenuItem;
use iterator\PancakeHouseMenu;
use iterator\Waitress;

require_once 'PancakeHouseMenu.php';
require_once 'DinnerMenu.php';
require_once 'MenuItem.php';
require_once 'Waitress.php';

$itemsPancake = [
	new MenuItem("K&B's Pancake Breakfast", 2.99, 'Pancake with scramble eggs and toast'),
	new MenuItem("Regurlar Pancake Breakfast", 2.99, 'Pancake with fried eggs, sausage'),
	new MenuItem("Blueberry Pancake", 3.49, 'Pancake made with fresh blueberries'),
	new MenuItem("Waffles", 3.59, 'Waffles, with your choice of blueberries or strawberries')
];
$pancakeHouseMenu = new PancakeHouseMenu($itemsPancake);

$itemsDinner = [
	new MenuItem('Vegetarian BLT', 2.99, '(Fakin) Bacon with lettuce & tomato on whole wheat', true),
	new MenuItem('BLT', 2.99, 'Bacon with lettuce tomato on whole wheat'),
	new MenuItem('Soup of the day', 3.29, 'soup of the day with a side of potato salad'),
	new MenuItem('Hotdog', 3.05, 'A Hotdog, with saurkraut, relish, onions, topped with cheese'),
	new MenuItem('Steamed Veggies and Brown rice', 3.99, 'Steamed veggies over brown rice', true),
	new MenuItem('Pasta', 3.89, 'Spaguetti marinara souce and slice of sourdough bread', true)
];
$dinnerMenu       = new DinnerMenu($itemsDinner);

$waitress = new Waitress($pancakeHouseMenu, $dinnerMenu);
$waitress->printMenu();