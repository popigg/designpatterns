<?php
/**
 * User: popigg
 * Date: 15/03/15
 * Time: 23:38
 */

namespace command2;


class Light
{
	var $room;

	public function __construct($room)
	{
		$this->room = $room;
	}

	public function on()
	{
		print($this->room . ' light is on.' . PHP_EOL);
	}

	public function off()
	{
		print($this->room. ' light is off.' . PHP_EOL);
	}
}