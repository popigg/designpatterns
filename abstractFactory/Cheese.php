<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 21:30
 */

namespace AbstractFactory;


interface Cheese
{
	public function getName();
}