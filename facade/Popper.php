<?php
/**
 * User: popigg
 * Date: 03/04/15
 * Time: 23:59
 */

namespace facade;


class Popper
{
	public function on()
	{
		print('Popcorn Popper on' . PHP_EOL);
	}

	public function pop()
	{
		print('Popcorn Popper popping popcorn!' . PHP_EOL);
	}

	public function off()
	{
		print('Popcorn Popper off' . PHP_EOL);
	}
}