<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:10
 */

namespace AbstractFactory;


class Garlic implements Veggie
{
	var $name;

	public function __construct()
	{
		$this->name = 'Garlic';
	}

	public function getName()
	{
		return $this->name;
	}
}