<?php
/**
 * User: popigg
 * Date: 05/04/15
 * Time: 22:08
 */

namespace template;


abstract class CaffeineBeverageWithHook
{
	final public function prepareRecipe()
	{
		$this->boilWater();
		$this->brew();
		$this->pourInCup();
		if ($this->customerWantsCondiments()) {
			$this->addCondiments();
		}
	}

	abstract function brew();

	abstract function addCondiments();

	public function boilWater()
	{
		print('Boiling water' . PHP_EOL);
	}

	public function pourInCup()
	{
		print('Pour into cup' . PHP_EOL);
	}

	public function customerWantsCondiments()
	{
		return true;
	}
}