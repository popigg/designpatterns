<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 23:31
 */

namespace iterator;

require_once 'MenuItem.php';
require_once 'PancakeHouseMenu.php';

class Waitress
{
	var $pancakeHouseMenu;
	var $dinnerMenu;

	public function __construct(PancakeHouseMenu $pancakeHouseMenu, DinnerMenu $dinnerMenu)
	{
		$this->pancakeHouseMenu = $pancakeHouseMenu;
		$this->dinnerMenu       = $dinnerMenu;
	}

	public function printMenu()
	{
		$pancakeIterator    = $this->pancakeHouseMenu->createIterator();
		$dinnerIterator     = $this->dinnerMenu->createIterator();

		print('MENU' . PHP_EOL . '----' . PHP_EOL .'BREAKFAST'. PHP_EOL);
		$this->printIterator($pancakeIterator);
		print('LUNCH' . PHP_EOL);
		$this->printIterator($dinnerIterator);
	}

	private function printIterator(Iterator $iterator)
	{
		while($iterator->hasNext()) {
			$menuItem = $iterator->next();
			print($menuItem->getName() . ', ');
			print($menuItem->getPrice() . ' -- ');
			print($menuItem->getDescription() . PHP_EOL);
		}
	}
}