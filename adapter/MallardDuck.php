<?php
/**
 * User: popigg
 * Date: 21/03/15
 * Time: 09:39
 */

namespace adapter;

require_once 'Duck.php';

class MallardDuck implements Duck
{
	public function quack()
	{
		print('Quack' . PHP_EOL);
	}

	public function fly()
	{
		print('I am flying' . PHP_EOL);
	}
}