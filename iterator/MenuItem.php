<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 23:45
 */

namespace iterator;


class MenuItem
{
	protected $name;
	protected $description;
	protected $price;
	protected $isVegetarian;

	public function __construct($name, $price, $description, $isVegetarian = false)
	{
		$this->name = $name;
		$this->description = $description;
		$this->price = $price;
		$this->isVegetarian = $isVegetarian;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return mixed
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @return mixed
	 */
	public function getIsVegetarian()
	{
		return $this->isVegetarian;
	}

}