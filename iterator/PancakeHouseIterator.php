<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 22:57
 */

namespace iterator;

require_once 'Iterator.php';

class PancakeHouseIterator implements Iterator
{
	var  $arrayList;

	public function __construct($items)
	{
		$this->arrayList = new \ArrayIterator();
		foreach ($items as $item) {
			$this->arrayList->append($item);
		}
	}

	public function hasNext()
	{
		return $this->arrayList->valid();
	}

	public function next()
	{
		$value = $this->arrayList->current();
		$this->arrayList->next();

		return $value;
	}
}