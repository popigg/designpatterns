<?php
/**
 * User: popigg
 * Date: 15/03/15
 * Time: 23:53
 */

namespace command4;


class Fan
{
	const HIGH      = 'high';
	const MEDIUM    = 'medium';
	const LOW       = 'low';
	const OFF       = 'off';

	var $room;
	var $speed;

	public function __construct($room)
	{
		$this->room = $room;
		$this->speed = self::OFF;
	}

	public function high()
	{
		$this->speed = self::HIGH;
		print($this->room . ' ceiling fan is on ' . $this->speed . PHP_EOL);
	}

	public function medium()
	{
		$this->speed = self::MEDIUM;
		print($this->room . ' ceiling fan is on ' . $this->speed . PHP_EOL);
	}

	public function low()
	{
		$this->speed = self::LOW;
		print($this->room . ' ceiling fan is on ' . $this->speed . PHP_EOL);
	}

	public function off()
	{
		$this->speed = self::OFF;
		print($this->room . ' ceiling fan is off.' . PHP_EOL);
	}

	public function getSpeed()
	{
		return $this->speed;
	}
}