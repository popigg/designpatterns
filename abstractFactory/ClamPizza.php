<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 13:31
 */

namespace AbstractFactory;


class ClamPizza extends Pizza
{
    var $ingredientFactory;

    public function __construct( $ingredientFactory )
    {
        $this->ingredientFactory = $ingredientFactory;
    }

    public function prepare()
    {
        print( 'Preparing :' . $this->getName() . PHP_EOL );
        $this->dough    = $this->ingredientFactory->createDough();
        $this->sauce    = $this->ingredientFactory->createSauce();
        $this->cheese   = $this->ingredientFactory->createCheese();
        $this->clam     = $this->ingredientFactory->createClam();
    }
}