<?php
/**
 * User: popigg
 * Date: 21/03/15
 * Time: 09:36
 */

namespace adapter;

interface Duck
{
	public function quack();
	public function fly();
}