<?php

namespace FactoryMethod;

require_once 'Pizza.php';

class ChicagoStyleCheesePizza extends Pizza
{
    function __construct () {
        $this->name 	= 'Chicago Style Deep Dish Cheese Pizza';
        $this->dough 	= 'Extra Thick Crust Dough';
        $this->sauce 	= 'Plum Tomato Sauce';
        array_push($this->toppings, 'Shredded Mozzarella Cheese');

        return $this;
    }

    public function cut()
    {
        print('Cutting the pizza into square slices' .  PHP_EOL);
    }
} 