<?php
/**
 * User: popigg
 * Date: 15/03/15
 * Time: 23:53
 */

namespace command2;

require_once 'Light.php';
require_once 'Command.php';


class LightOffCommand implements Command
{
	var $light;

	public function __construct(Light $light)
	{
		$this->light = $light;
	}
	public function execute()
	{
		$this->light->off();
	}
}