<?php
/**
 * User: popigg
 * Date: 08/03/15
 * Time: 16:00
 */

use command\Light;
use command\LightOnCommand;
use command\SimpleRemoteControl;

require_once 'SimpleRemoteControl.php';
require_once 'Light.php';
require_once 'LightOnCommand.php';

$remote     = new SimpleRemoteControl();
$light      = new Light();
$lightOn    = new LightOnCommand( $light );

$remote->setCommand( $lightOn );
$remote->buttonWasPressed();