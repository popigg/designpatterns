<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 23:15
 */

namespace command2;

require_once 'RemoteControl.php';
require_once 'Light.php';
require_once 'Fan.php';
require_once 'Stereo.php';
require_once 'LightOnCommand.php';
require_once 'LightOffCommand.php';
require_once 'FanOnCommand.php';
require_once 'FanOffCommand.php';
require_once 'StereoOnWithCDCommand.php';
require_once 'StereoOffCommand.php';

$remoteControl = new RemoteControl();

$livingRoomLight    = new Light('Living Room');
$kitchenLight       = new Light('kitchen');

$ceilingFan         = new Fan('Living Room');

$stereo             = new Stereo('Living Room');

$livingRoomLightOn  = new LightOnCommand($livingRoomLight);
$livingRoomLightOff = new LightOffCommand($livingRoomLight);

$kitchenLightOn     = new LightOnCommand($kitchenLight);
$kitchenLightOff    = new LightOffCommand($kitchenLight);

$ceilingFanOn       = new FanOnCommand($ceilingFan);
$ceilingFanOff      = new FanOffCommand($ceilingFan);

$stereoOnWithCD     = new StereoOnWithCDCommand($stereo);
$stereoOff          = new StereoOffCommand($stereo);

$remoteControl->setCommand(0, $livingRoomLightOn, $livingRoomLightOff);
$remoteControl->setCommand(1, $kitchenLightOn, $kitchenLightOff);
$remoteControl->setCommand(2, $ceilingFanOn, $ceilingFanOff);
$remoteControl->setCommand(3, $stereoOnWithCD, $stereoOff);

print($remoteControl);

$remoteControl->onButtonWasPressed(0);
$remoteControl->offButtonWasPressed(0);

$remoteControl->onButtonWasPressed(1);
$remoteControl->offButtonWasPressed(1);

$remoteControl->onButtonWasPressed(2);
$remoteControl->offButtonWasPressed(2);

$remoteControl->onButtonWasPressed(3);
$remoteControl->offButtonWasPressed(3);