<?php

namespace FactoryMethod;

abstract class PizzaStore 
{
	public function orderPizza( $item )
	{
        $pizza = $this->createPizza( $item );

		$pizza->prepare();
		$pizza->bake();
		$pizza->cut();
		$pizza->box();

		return $pizza;
	}

	abstract function createPizza( $item );
}