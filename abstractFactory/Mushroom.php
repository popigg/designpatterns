<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:11
 */

namespace AbstractFactory;


class Mushroom implements Veggie
{
	var $name;

	public function __construct()
	{
		$this->name = 'Mushrooms';
	}

	public function getName()
	{
		return $this->name;
	}
}