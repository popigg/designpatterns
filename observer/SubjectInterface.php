<?php

namespace Observer;

Interface Subject {

	public function registerObserver(Observer $observer);

	public function removeObserver(Observer $observer);

	public function notifyObservers();
} 