<?php

namespace FactoryMethod;

class Pizza
{	
	var $name;

	var $dough;

	var $sauce;

	var $toppings = [];

	public function prepare()
	{
		print( 'Preparing ' . $this->name . PHP_EOL);
		print( 'Tossing ' . $this->dough . PHP_EOL);
		print( 'Adding ' . $this->sauce . PHP_EOL);
		print( 'Toppings: ' . PHP_EOL);

		foreach ($this->toppings as $toppin) {
			print(' ' . $toppin);
		}

        print(PHP_EOL);
	}

	public function bake()
	{
		print('Bake for 25 mins at 350' . PHP_EOL);
	}

	public function cut()
	{
		print('Cutting the pizza into diagonal slices' .  PHP_EOL);
	}

	public function box()
	{
		print('Place pizza in official PizzaStore box' . PHP_EOL);
	}

	public function getName()
	{
		return $this->name;
	}
}