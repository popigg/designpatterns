<?php
/**
 * User: popigg
 * Date: 21/03/15
 * Time: 09:42
 */

namespace adapter;


interface Turkey
{
	public function gobble();
	public function fly();
}