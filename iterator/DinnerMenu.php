<?php
/**
 * User: popigg
 * Date: 18/04/15
 * Time: 23:17
 */

namespace iterator;

require_once 'DinnerMenuIterator.php';

class DinnerMenu
{
	static $maxItems = 6;
	var $numOfItems = 0;
	var $items;

	public function __construct($items)
	{
		$this->items = $items;
	}

	public function createIterator()
	{
		return new DinnerMenuIterator($this->items);
	}
}