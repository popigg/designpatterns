<?php

namespace Decorator;

abstract class Beverage
{
    var $description;

    public function __construct() {
        $this->description = 'Unknown Beverage';
    }

    public function getDescription() {
        return $this->description;
    }

    public abstract function cost();

} 