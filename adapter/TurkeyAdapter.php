<?php
/**
 * User: popigg
 * Date: 21/03/15
 * Time: 10:04
 */

namespace adapter;


class TurkeyAdapter implements Duck
{
	var $turkey;

	public function __construct(Turkey $turkey)
	{
		$this->turkey = $turkey;
	}

	public function quack()
	{
		$this->turkey->gobble();
	}

	public function fly()
	{
		for($i = 1; $i < 6; $i++) {
			$this->turkey->fly();
		}
	}
}