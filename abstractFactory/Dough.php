<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 21:28
 */

namespace AbstractFactory;


interface Dough
{
	public function getName();
}