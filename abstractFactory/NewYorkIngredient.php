<?php

/**
 * User: popigg
 * Date: 22/02/15
 * Time: 11:53
 */

namespace AbstractFactory;

include_once 'PizzaIngredientInterface.php';
include_once 'ThinCrustDough.php';
include_once 'MarinaraSauce.php';
include_once 'ReggianoCheese.php';
include_once 'RedPepper.php';

class NewYorkIngredient implements PizzaIngredientInterface
{
    public function createDough()
    {
        return new ThinCrustDough();
    }

    public function createSauce()
    {
        return new MarinaraSauce();
    }

    public function createCheese()
    {
        return new ReggianoCheese();
    }

    public function createVeggies()
    {
        $veggies = [ new Garlic(), new Onion(), new Mushroom(), new RedPepper() ];
        return $veggies;
    }

    public function createPepperoni()
    {
        return new SlicedPepperoni();
    }

    public function createClam()
    {
        return new FreshClam();
    }
}