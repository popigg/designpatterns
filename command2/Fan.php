<?php
/**
 * User: popigg
 * Date: 15/03/15
 * Time: 23:53
 */

namespace command2;


class Fan
{
	var $room;

	public function __construct($room)
	{
		$this->room = $room;
	}

	public function onHigh()
	{
		print($this->room . ' ceiling fan is on high.' . PHP_EOL);
	}

	public function off()
	{
		print($this->room . ' ceiling fan is off.' . PHP_EOL);
	}
}