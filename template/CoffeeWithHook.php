<?php
/**
 * User: popigg
 * Date: 05/04/15
 * Time: 22:13
 */

namespace template;

require_once 'CaffeineBeverageWithHook.php';

class CoffeeWithHook extends CaffeineBeverageWithHook
{
	public function brew()
	{
		print('Dripping coffee through filter' . PHP_EOL);
	}

	public function addCondiments()
	{
		print('Adding sugar and Milk' . PHP_EOL);
	}

	public function customerWantsCondiments()
	{
		$answer = $this->getUserInput();

		if ( substr(strtolower($answer), 0, 1) === 'y' ) {
			return true;
		}
		return false;
	}

	private function getUserInput()
	{
		$answer =  null;

		print( 'Would you like milk and sugar with your coffee [y/n]: ');
		$handle = fopen ("php://stdin","r");
		$line   = fgets($handle);

		try {
			$answer = trim($line);
		} catch (\Exception $e ) {
			print($e->getMessage());
		}

		return $answer;
	}
}