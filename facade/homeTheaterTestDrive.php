<?php
/**
 * User: popigg
 * Date: 03/04/15
 * Time: 23:20
 */

namespace facade;

require_once('Amplifier.php');
require_once('Dvd.php');
require_once('Projector.php');
require_once('Screen.php');
require_once('Light.php');
require_once('Popper.php');
require_once('HomeTheaterFacade.php');

$amp        = new Amplifier();
$dvd        = new Dvd();
$projector  = new Projector();
$screen     = new Screen();
$lights     = new Light();
$popper     = new Popper();

$homeTheater = new HomeTheaterFacade($amp, null, $dvd, null, $projector, $lights, $screen, $popper);

$homeTheater->watchMovie('Riders of the Lost Ark');
$homeTheater->endMovie();