<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 00:05
 */

namespace command2;

require_once 'Command.php';
require_once 'Stereo.php';

class StereoOnWithCDCommand implements Command
{
	var $stereo;

	public function __construct(Stereo $stereo)
	{
		$this->stereo = $stereo;
	}

	public function execute()
	{
		$this->stereo->on();
		$this->stereo->setCD();
		$this->stereo->setVolume(11);
	}
}