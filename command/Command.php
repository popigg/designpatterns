<?php
/**
 * User: popigg
 * Date: 07/03/15
 * Time: 23:54
 */

namespace command;

interface Command
{
	public function execute();
}