<?php

namespace FactoryMethod;

require_once 'Pizza.php';

class NewYorkStyleCheesePizza extends Pizza
{
	function __construct () {
		$this->name 	= 'NY Style Sauce and Cheese Pizza';
		$this->dough 	= 'Thin crust Dough';
		$this->sauce 	= 'Marinara Sauce';
		array_push($this->toppings, 'Grated Reggiano Cheese');

        return $this;
	}
}