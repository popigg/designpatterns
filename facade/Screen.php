<?php
/**
 * User: popigg
 * Date: 04/04/15
 * Time: 00:04
 */

namespace facade;


class Screen
{
	public function down()
	{
		print('Theater screen going down' . PHP_EOL);
	}

	public function up()
	{
		print('Theater screen going up' . PHP_EOL);
	}
}