<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 23:15
 */

namespace command4;

require_once 'RemoteControl.php';

require_once 'Fan.php';
require_once 'FanHighCommand.php';
require_once 'FanMediumCommand.php';
require_once 'FanLowCommand.php';
require_once 'FanOffCommand.php';

$remoteControl = new RemoteControl();

$ceilingFan         = new Fan('Living Room');


$ceilingFanMedium   = new FanMediumCommand($ceilingFan);
$ceilingFanHigh     = new FanHighCommand($ceilingFan);
$ceilingFanOff      = new FanOffCommand($ceilingFan);


$remoteControl->setCommand(0, $ceilingFanMedium, $ceilingFanOff);
$remoteControl->setCommand(1, $ceilingFanHigh, $ceilingFanOff);


$remoteControl->onButtonWasPressed(0);
$remoteControl->offButtonWasPressed(0);

print($remoteControl);

$remoteControl->undoButtonWasPressed();

$remoteControl->onButtonWasPressed(1);

print($remoteControl);

$remoteControl->undoButtonWasPressed();
