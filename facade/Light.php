<?php
/**
 * User: popigg
 * Date: 04/04/15
 * Time: 00:02
 */

namespace facade;


class Light
{
	public function on()
	{
		print('Theater Ceiling lights on' . PHP_EOL);
	}

	public function dim( $percentage )
	{
		print('Theater Ceiling lights dimming to ' . $percentage . '%' . PHP_EOL);
	}
}