<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:11
 */

namespace AbstractFactory;


class Onion implements Veggie
{
	var $name;

	public function __construct()
	{
		$this->name = 'Onion';
	}

	public function getName()
	{
		return $this->name;
	}
}