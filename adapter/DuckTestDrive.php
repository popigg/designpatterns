<?php
/**
 * User: popigg
 * Date: 21/03/15
 * Time: 10:10
 */

namespace adapter;

require_once 'MallardDuck.php';
require_once 'WildTurkey.php';
require_once 'TurkeyAdapter.php';

$duck = new MallardDuck();

$turkey = new WildTurkey();
$turkeyAdapter = new TurkeyAdapter($turkey);

print('The Turkey says ...' . PHP_EOL);
$turkey->gobble();
$turkey->fly();

print('The Duck says ...' . PHP_EOL);
$duck->quack();
$duck->fly();

print('The Turkey Adapter says ...' . PHP_EOL);
$turkeyAdapter->quack();
$turkeyAdapter->fly();