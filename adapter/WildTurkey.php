<?php
/**
 * User: popigg
 * Date: 21/03/15
 * Time: 09:43
 */

namespace adapter;

require_once 'Turkey.php';

class WildTurkey implements Turkey
{
	public function gobble()
	{
		print('Gobble gobble' . PHP_EOL);
	}

	public function fly()
	{
		print('I am flying a short distance' . PHP_EOL);
	}
}