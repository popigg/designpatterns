<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:01
 */

namespace AbstractFactory;

include_once 'Cheese.php';

class ReggianoCheese implements Cheese
{
	var $name;

	public function __construct()
	{
		$this->name = 'Grated Reggiano Cheese';
	}

	public function getName()
	{
		return $this->name;
	}

}