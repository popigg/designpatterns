<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 22:53
 */

namespace command2;


require_once 'Command.php';
require_once 'Fan.php';

class FanOffCommand implements Command
{
	var $fan;

	public function __construct(Fan $fan)
	{
		$this->fan = $fan;
	}

	public function execute()
	{
		$this->fan->off();
	}
}