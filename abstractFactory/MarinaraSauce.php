<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:06
 */

namespace AbstractFactory;

include_once 'Sauce.php';

class MarinaraSauce implements Sauce
{
	var $name;

	public function __construct()
	{
		$this->name = 'Marinara Sauce';
	}

	public function getName()
	{
		return $this->name;
	}
}