<?php
/**
 * User: popigg
 * Date: 03/04/15
 * Time: 23:37
 */

namespace facade;

class HomeTheaterFacade
{
		var $amp;
		var $tuner;
		var $dvd;
		var $cd;
		var $projector;
		var $lights;
		var $screen;
		var $popper;

	public function __construct(Amplifier $amp, $tuner, Dvd $dvd, $cd, Projector $projector,
	                            Light $lights, Screen $screen, Popper $popper)
	{

		$this->amp          = $amp;
		$this->tuner        = $tuner;
		$this->dvd          = $dvd;
		$this->cd           = $cd;
		$this->projector    = $projector;
		$this->lights       = $lights;
		$this->screen       = $screen;
		$this->popper       = $popper;

	}

	public function watchMovie( $movie )
	{
		print('Get ready to watch a movie ...' . PHP_EOL);
		$this->popper->on();
		$this->popper->pop();
		$this->lights->dim( 10 );
		$this->screen->down();
		$this->projector->on();
		$this->projector->wideScreenMode();
		$this->amp->on();
		$this->amp->setDvd( $this->dvd );
		$this->amp->setSurroundSound();
		$this->amp->setVolume( 5 );
		$this->dvd->on();
		$this->dvd->play( $movie );
	}

	public function endMovie()
	{
		print('Shutting movie theater down ...' . PHP_EOL);
		$this->popper->off();
		$this->lights->on();
		$this->screen->up();
		$this->projector->off();
		$this->amp->off();
		$this->dvd->stop();
		$this->dvd->eject();
		$this->dvd->off();
	}
}