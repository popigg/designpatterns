<?php
namespace Decorator;

/**
 * Created by PhpStorm.
 * User: popigg
 * Date: 01/12/14
 * Time: 18:41
 */

require_once 'Beverage.php';
require_once 'DarkRoast.php';
require_once 'Decaf.php';
require_once 'HouseBlend.php';
require_once 'Expresso.php';

require_once 'CondimentsDecorator.php';
require_once 'Mocha.php';
require_once 'Soy.php';
require_once 'SteamedMilk.php';
require_once 'Whip.php';

$beverage = new Expresso();

print( $beverage->getDescription() . ' € ' . $beverage->cost() . PHP_EOL);

$beverage2 = new DarkRoast();
$beverage2 = new Mocha($beverage2);
$beverage2 = new Mocha($beverage2);
$beverage2 = new Whip($beverage2);

print( $beverage2->getDescription() . ' € ' . $beverage2->cost() . PHP_EOL);

$beverage3 = new HouseBlend();
$beverage3 = new Soy($beverage3);
$beverage3 = new Mocha($beverage3);
$beverage3 = new Whip($beverage3);

print( $beverage3->getDescription() . ' € ' . $beverage3->cost() . PHP_EOL);

