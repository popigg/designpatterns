<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 11:55
 */

namespace AbstractFactory;

include_once 'Dough.php';

class ThinCrustDough implements Dough
{
	var $name;

	public function __construct()
	{
		$this->name = 'Thin Crust Dough';
	}

	public function getName()
	{
		return $this->name;
	}
}