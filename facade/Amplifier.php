<?php
/**
 * User: popigg
 * Date: 04/04/15
 * Time: 00:08
 */

namespace facade;


class Amplifier
{
	public function on()
	{
		print('Top-O-Line Amplifier on' . PHP_EOL);
	}

	public function setDvd( $dvd )
	{
		print('Top-O-Line Amplifier setting Dvd Player to ' . $dvd->getName() . PHP_EOL);
	}

	public function setSurroundSound()
	{
		print('Top-O-Line Amplifier surround sound on (5 speakers, 1 subwoofer)' . PHP_EOL);
	}

	public function setVolume( $volume )
	{
		print('Top-O-Line Amplifier setting volume to ' . $volume . PHP_EOL);
	}

	public function off()
	{
		print('Top-O-Line Amplifier off' . PHP_EOL);
	}
}