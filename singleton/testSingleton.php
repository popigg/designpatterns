<?php
/**
 * User: popigg
 * Date: 03/03/15
 * Time: 23:38
 */

use singleton\Singleton;
use singleton\SingletonChild;

require_once 'Singleton.php';
require_once 'SingletonChild.php';

$obj = Singleton::getInstance();
var_dump($obj === Singleton::getInstance());            // bool(true)

$obj2 = Singleton::getInstance();
var_dump($obj2 === Singleton::getInstance());           // bool(true)

var_dump($obj === $obj2);                               // bool(true)

$anotherObj = SingletonChild::getInstance();
var_dump($anotherObj === Singleton::getInstance());     // bool(false)

var_dump($anotherObj === SingletonChild::getInstance());// bool(true)