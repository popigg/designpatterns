<?php

namespace Observer;

interface Display {
	
	public function display();
}