<?php

namespace Decorator;

class Soy extends CondimentsDecorator
{
    var $beverage;

    public function __construct(Beverage $beverage) {
        $this->beverage = $beverage;
    }

    public function getDescription() {
        return $this->beverage->getDescription() . ', Soy';
    }

    public function cost() {
        return $this->beverage->cost() + 0.15;
    }
} 