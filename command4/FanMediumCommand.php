<?php
/**
 * User: popigg
 * Date: 16/03/15
 * Time: 22:52
 */

namespace command4;

require_once 'Command.php';
require_once 'Fan.php';

class FanMediumCommand implements Command
{
	var $fan;
	var $prevSpeed;

	public function __construct(Fan $fan)
	{
		$this->fan = $fan;
	}

	public function execute()
	{
		$this->prevSpeed = $this->fan->getSpeed();
		$this->fan->medium();
	}

	public  function undo()
	{
		switch( $this->prevSpeed ) {
			case fan::HIGH: $this->fan->high();
				break;

			case fan::MEDIUM: $this->fan->medium();
				break;

			case fan::LOW: $this->fan->low();
				break;

			case fan::OFF: $this->fan->off();
				break;

		}
	}
}