<?php
/**
 * User: popigg
 * Date: 05/04/15
 * Time: 22:26
 */

use template\CoffeeWithHook;
use template\TeaWithHook;

require_once 'TeaWithHook.php';
require_once 'CoffeeWithHook.php';

$teaHook        = new TeaWithHook();
$coffeeWithHook = new CoffeeWithHook();

print('Making tea ...' . PHP_EOL);
$teaHook->prepareRecipe();

print('Making coffee ...' . PHP_EOL);
$coffeeWithHook->prepareRecipe();