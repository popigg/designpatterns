<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 14:41
 */

namespace AbstractFactory;

require_once 'NewYorkPizzaStore.php';
//require_once 'ChicagoPizzaStore.php';

$nyStore 		= new NewYorkPizzaStore();
//$chicagoStore 	= new ChicagoPizzaStore();

$pizza 			= $nyStore->orderPizza('cheese');
print('Ethan ordered a ' . $pizza->getName() . PHP_EOL);

print(PHP_EOL);

//$pizza 			= $chicagoStore->orderPizza('cheese');
//print('Joel ordered a ' . $pizza->getName() . PHP_EOL );