<?php

namespace Observer;


class StaticticsDisplay implements Observer, Display{

    private $maxTemp = '0.0f';
    private $minTemp = 200;
    private $tempSum = '0.0f';
    private $numReadings;

    function __construct (Weather $weather) {
        $weather->registerObserver($this);
    }

    public function update($temp, $hum, $pres) {
        $this->tempSum += $temp;
        $this->numReadings++;

        if ($temp > $this->maxTemp) {
            $this->maxTemp = $temp;
        }

        if ($temp < $this->minTemp) {
            $this->minTemp = $temp;
        }

        $this->display();
    }
    
    public function display() {
        print("Avg/Max/Min temperature = " . ($this->tempSum / $this->numReadings) . "/"
            . $this->maxTemp . "/" . $this->minTemp . "\xA");
    }
} 