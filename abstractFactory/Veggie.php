<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 11:56
 */

namespace AbstractFactory;

interface Veggie
{
	public function getName();
}