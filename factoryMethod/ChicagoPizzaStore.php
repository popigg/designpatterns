<?php

namespace FactoryMethod;

require_once 'PizzaStore.php';
require_once 'ChicagoStyleCheesePizza.php';

class ChicagoPizzaStore extends PizzaStore
{

    public function createPizza( $item )
    {
        switch ( $item ) {
            case 'cheese':
                return new ChicagoStyleCheesePizza();
                break;

//			 case 'veggie':
//			 	return new ChicagoStyleVeggiePizza();
//			 	break;
//
//			 case 'clam':
//			 	return new ChicagoStyleClamPizza();
//			 	break;
//
//			 case 'pepperoni':
//			 	return new ChicagoStylePepperoniPizza();
//			 	break;

            default:
                # code...
                break;
        }
    }

} 