<?php
/**
 * User: popigg
 * Date: 04/04/15
 * Time: 00:15
 */

namespace facade;


class Dvd
{
	var $movie;

	public function getName()
	{
		return 'Top-O-Line DVD Player';
	}

	public function on()
	{
		print( $this->getName() . ' on ' . PHP_EOL );
	}

	public function play( $movie )
	{
		$this->movie = $movie;
		print( $this->getName() . ' playing "' . $movie .'"' . PHP_EOL);
	}

	public function stop()
	{
		print( $this->getName() . ' stop ' . '"' . $this->movie . '"' . PHP_EOL );
	}

	public function eject()
	{
		print( $this->getName() . ' eject ' . PHP_EOL );
	}

	public function off()
	{
		print( $this->getName() . ' off ' . PHP_EOL );
	}
}