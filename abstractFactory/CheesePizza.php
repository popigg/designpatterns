<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 13:25
 */

namespace AbstractFactory;

include_once 'Pizza.php';

class CheesePizza extends Pizza
{
    var $ingredientFactory;

    public function __construct( $ingredientFactory )
    {
        $this->ingredientFactory = $ingredientFactory;
    }

    public function prepare()
    {
        print( 'Preparing :' . $this->getName() . PHP_EOL );
        $this->dough    = $this->ingredientFactory->createDough();
		print( 'Tossing: ' . $this->dough->getName() . PHP_EOL );

        $this->sauce    = $this->ingredientFactory->createSauce();
	    print( 'Adding: ' . $this->sauce->getName() . PHP_EOL );

        $this->cheese   = $this->ingredientFactory->createCheese();
	    print( 'Cheese: ' . $this->cheese->getName() . PHP_EOL );
    }
}