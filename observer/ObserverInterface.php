<?php

namespace Observer;

interface Observer {

	public function	update($temperature, $humidity, $pressure);
}