<?php
/**
 * User: popigg
 * Date: 15/03/15
 * Time: 23:58
 */

namespace command2;


class Stereo
{
	var $room;

	public function __construct($room)
	{
		$this->room = $room;
	}

	public function on()
	{
		print($this->room . ' stereo is on.' . PHP_EOL);
	}

	public function setCD()
	{
		print($this->room . ' stereo is set for CD input.' . PHP_EOL);
	}

	public function setVolume($vol)
	{
		print($this->room . ' stereo volume set to ' . $vol . '.' . PHP_EOL);
	}

	public function off()
	{
		print($this->room . ' stereo is off.' . PHP_EOL);
	}
}