<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:03
 */

namespace AbstractFactory;

include_once 'Veggie.php';

class RedPepper implements Veggie
{
	var $name;

	public function __construct()
	{
		$this->name = 'Red Pepper';
	}

	public function getName()
	{
		return $this->name;
	}
}