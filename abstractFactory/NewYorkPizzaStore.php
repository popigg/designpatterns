<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 13:36
 */

namespace AbstractFactory;

include_once 'PizzaStore.php';
include_once 'NewYorkIngredient.php';
include_once 'CheesePizza.php';

class NewYorkPizzaStore extends PizzaStore
{

    public function createPizza( $item )
    {
	    $pizza              = null;
        $ingredientFactory  = new NewYorkIngredient();

	    switch ( $item ) {
		    case 'cheese':
				$pizza = new CheesePizza( $ingredientFactory );
				$pizza->setName( 'New York Style Cheese Pizza' );
			    break;

		    case 'clam':
			    $pizza = new ClamPizza( $ingredientFactory );
			    $pizza->setName( 'New York Style Clam Pizza' );
			    break;

		    default:
			    # code...
			    break;
	    }

	    return $pizza;
    }

}