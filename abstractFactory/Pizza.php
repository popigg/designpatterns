<?php
/**
 * User: popigg
 * Date: 22/02/15
 * Time: 12:35
 */

namespace AbstractFactory;


abstract class Pizza
{
    var $name;
    var $dough;
    var $sauce;
    var $veggies = [];
    var $cheese;
    var $pepporoni;
    var $clam;

    public abstract function prepare();

    public function bake()
    {
        print('Bake for 25 mins at 350' . PHP_EOL);
    }

    public function cut()
    {
        print('Cutting the pizza into diagonal slices' .  PHP_EOL);
    }

    public function box()
    {
        print('Place pizza in official PizzaStore box' . PHP_EOL);
    }

    public function setName( $name )
    {
        return $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}