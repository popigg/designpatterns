<?php
/**
 * User: popigg
 * Date: 07/03/15
 * Time: 23:55
 */

namespace command;


class Light
{

	public function on()
	{
		print('Light is on' . PHP_EOL);
	}

	public function off()
	{
		print('Light is off' . PHP_EOL);
	}
}