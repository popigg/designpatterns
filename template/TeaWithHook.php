<?php
/**
 * User: popigg
 * Date: 05/04/15
 * Time: 22:13
 */

namespace template;

require_once 'CaffeineBeverageWithHook.php';

class TeaWithHook extends CaffeineBeverageWithHook
{
	public function brew()
	{
		print('Steeping the tea' . PHP_EOL);
	}

	public function addCondiments()
	{
		print('Adding Lemon' . PHP_EOL);
	}

	public function customerWantsCondiments()
	{
		$answer = $this->getUserInput();

		if ( substr(strtolower($answer), 0, 1) === 'y' ) {
			return true;
		}
		return false;
	}

	private function getUserInput()
	{
		$answer =  null;

		print( 'Would you like lemon with your tea [y/n]: ');
		$handle = fopen ("php://stdin","r");
		$line   = fgets($handle);

		try {
			$answer = trim($line);
		} catch (\Exception $e ) {
			print($e->getMessage());
		}

		return $answer;
	}
}