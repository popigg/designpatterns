<?php

namespace Observer;

class Weather implements Subject {

    private $observerList;

    private $temperature;

    private $humidity;

    private $pressure;

    function __construct() {
        $this->observerList = [];
    }

    public function registerObserver(Observer $observer) {
        array_push($this->observerList, $observer);
    }

    public function removeObserver(Observer $observer) {
        foreach ($this->observerList as $key => $value) {
            if ($value == $observer) {
                unset($this->observerList[$key]);
            }
        }
    }

    public function notifyObservers() {
        foreach ($this->observerList as $index => $observer) {
            $observer->update($this->temperature, $this->humidity, $this->pressure);
        }
    }

    public function measurementChanged() {
        $this->notifyObservers();
    }

    public function setMeasurements($temp, $hum, $pres) {
        $this->temperature  = $temp;
        $this->humidity     = $hum;
        $this->pressure     = $pres;

        $this->measurementChanged();
    }

}