<?php
/**
 * User: popigg
 * Date: 03/03/15
 * Time: 23:16
 */

namespace singleton;

class Singleton {

	public static function getInstance() {
		static $singleton = null;
		if (null === $singleton) {
			$singleton = new static();
		}

		return $singleton;
	}

	protected function __construct() {

	}

	private function __clone() {

	}
}