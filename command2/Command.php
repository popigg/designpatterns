<?php
/**
 * User: popigg
 * Date: 15/03/15
 * Time: 23:15
 */

namespace command2;


interface Command
{
	public function execute();
}