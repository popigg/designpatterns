<?php

namespace Observer;


class CurrentConditionsDisplay implements Observer, Display {

    private $temperature;

    private $humidity;

    function __construct (Weather $weather) {
        $weather->registerObserver($this);
    }

    public function update($temp, $hum, $pres) {
        $this->temperature = $temp;

        $this->humidity    = $hum;

        $this->display();
    }

    public function display() {
        print('current conditions: ' . $this->temperature . ' F degrees and ' . $this->humidity . ' % humidity' . "\xA");
    }
} 